{
    "id": "2d3c62cf-1630-444d-a34b-4738254d6010",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_platform",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f06ed80-50df-4051-8741-3b62f34007df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d3c62cf-1630-444d-a34b-4738254d6010",
            "compositeImage": {
                "id": "26cc0fce-f4e1-40e4-8250-9a5edfc98e8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f06ed80-50df-4051-8741-3b62f34007df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b72642d-4cf9-4eaf-81d3-ca86732a31c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f06ed80-50df-4051-8741-3b62f34007df",
                    "LayerId": "2751d141-4c36-437d-853f-331ae48a07d2"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 16,
    "layers": [
        {
            "id": "2751d141-4c36-437d-853f-331ae48a07d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d3c62cf-1630-444d-a34b-4738254d6010",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 45,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}