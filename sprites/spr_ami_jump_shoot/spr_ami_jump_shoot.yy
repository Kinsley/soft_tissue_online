{
    "id": "d99e5e91-daf5-4d96-8305-fa2da2b8d2c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ami_jump_shoot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 28,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c47c57fd-97da-48e3-a37c-7470e0ade6e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d99e5e91-daf5-4d96-8305-fa2da2b8d2c6",
            "compositeImage": {
                "id": "598d8124-ef91-43ca-9db6-07642896f0d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c47c57fd-97da-48e3-a37c-7470e0ade6e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91c80422-bd66-43e6-b804-6c6bd5bd1629",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c47c57fd-97da-48e3-a37c-7470e0ade6e4",
                    "LayerId": "90b41c7e-1518-4002-b7cc-731b1e62b1b7"
                }
            ]
        },
        {
            "id": "42e7aed1-0773-4a72-bfe8-48663638db6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d99e5e91-daf5-4d96-8305-fa2da2b8d2c6",
            "compositeImage": {
                "id": "90c25cf2-62cd-457f-980a-36e9e9c2d69a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42e7aed1-0773-4a72-bfe8-48663638db6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9781c540-101c-46fb-832f-9d2e1aef1fe2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42e7aed1-0773-4a72-bfe8-48663638db6f",
                    "LayerId": "90b41c7e-1518-4002-b7cc-731b1e62b1b7"
                }
            ]
        },
        {
            "id": "ad8dcd10-e076-4650-8ae5-06ab551eef50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d99e5e91-daf5-4d96-8305-fa2da2b8d2c6",
            "compositeImage": {
                "id": "975e7eac-f41d-4ca1-b72c-7c6e4e6e02ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad8dcd10-e076-4650-8ae5-06ab551eef50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "676701c5-b3ff-4183-b835-e89846bc3158",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad8dcd10-e076-4650-8ae5-06ab551eef50",
                    "LayerId": "90b41c7e-1518-4002-b7cc-731b1e62b1b7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "90b41c7e-1518-4002-b7cc-731b1e62b1b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d99e5e91-daf5-4d96-8305-fa2da2b8d2c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}