{
    "id": "7a83ce88-ec2c-4e02-9507-265e2777a9c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_data_logo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f926b6e-4be5-4b73-801e-ddfaff293032",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a83ce88-ec2c-4e02-9507-265e2777a9c3",
            "compositeImage": {
                "id": "ba700621-6655-4847-bd01-d48457b1c3ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f926b6e-4be5-4b73-801e-ddfaff293032",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d824e9e8-8281-4fe7-ba2a-2605cde0fd47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f926b6e-4be5-4b73-801e-ddfaff293032",
                    "LayerId": "e99dddcb-e2b8-40e6-8923-cb61b535aab0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e99dddcb-e2b8-40e6-8923-cb61b535aab0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a83ce88-ec2c-4e02-9507-265e2777a9c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}