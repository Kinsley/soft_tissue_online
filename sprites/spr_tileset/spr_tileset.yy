{
    "id": "27b1315c-868f-44df-98eb-51fcb1f38073",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 116,
    "bbox_left": 16,
    "bbox_right": 159,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cabf9edd-7fe0-4dd8-912e-c22a70261daf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27b1315c-868f-44df-98eb-51fcb1f38073",
            "compositeImage": {
                "id": "4f6dafad-b347-4c24-a6ba-1f30cf2da402",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cabf9edd-7fe0-4dd8-912e-c22a70261daf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce73ac00-10fc-4413-bf08-c9f36d17bfa1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cabf9edd-7fe0-4dd8-912e-c22a70261daf",
                    "LayerId": "287b8b86-2342-41ed-8a3c-ed7dce9bc536"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "287b8b86-2342-41ed-8a3c-ed7dce9bc536",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "27b1315c-868f-44df-98eb-51fcb1f38073",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}