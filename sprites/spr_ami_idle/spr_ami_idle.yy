{
    "id": "f606057f-7ccd-46bb-827c-220bc1423793",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ami_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 11,
    "bbox_right": 20,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c11d6fc-0db3-498f-82fe-6db30a13e1c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f606057f-7ccd-46bb-827c-220bc1423793",
            "compositeImage": {
                "id": "c419a233-4f81-4d7d-9329-1e8d358449b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c11d6fc-0db3-498f-82fe-6db30a13e1c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "188a6c8b-2f85-40c9-b3ea-fd85034044a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c11d6fc-0db3-498f-82fe-6db30a13e1c8",
                    "LayerId": "4abb6cd4-eacc-4d22-83f9-97104167f651"
                }
            ]
        },
        {
            "id": "57ef44e0-ecdf-46f4-af44-8334e3d96f79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f606057f-7ccd-46bb-827c-220bc1423793",
            "compositeImage": {
                "id": "eb5bbb5e-74e3-4772-837f-61f2eee950e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57ef44e0-ecdf-46f4-af44-8334e3d96f79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4bed70b-8a5e-48e9-81b0-3a73c2405fdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57ef44e0-ecdf-46f4-af44-8334e3d96f79",
                    "LayerId": "4abb6cd4-eacc-4d22-83f9-97104167f651"
                }
            ]
        },
        {
            "id": "abe24a95-6e06-4967-ad0d-17bf5be61e4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f606057f-7ccd-46bb-827c-220bc1423793",
            "compositeImage": {
                "id": "1b624eaf-1db4-4bfb-b9ec-b1c7226039c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abe24a95-6e06-4967-ad0d-17bf5be61e4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3030551c-ead6-4a49-84cf-3d1618507c1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abe24a95-6e06-4967-ad0d-17bf5be61e4a",
                    "LayerId": "4abb6cd4-eacc-4d22-83f9-97104167f651"
                }
            ]
        },
        {
            "id": "eb1ae619-da03-4bf3-b99f-5e0a46364651",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f606057f-7ccd-46bb-827c-220bc1423793",
            "compositeImage": {
                "id": "5b4e39f5-edfb-4b38-bc28-cdf896c9c68d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb1ae619-da03-4bf3-b99f-5e0a46364651",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abb6e9ac-7409-4417-818b-16b4e664fcf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb1ae619-da03-4bf3-b99f-5e0a46364651",
                    "LayerId": "4abb6cd4-eacc-4d22-83f9-97104167f651"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4abb6cd4-eacc-4d22-83f9-97104167f651",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f606057f-7ccd-46bb-827c-220bc1423793",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}