{
    "id": "fd1c1d09-8de8-494d-8261-0c71eafbaa77",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sound_player_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0acfc0b8-f677-4bda-8a90-243fb0b36cc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd1c1d09-8de8-494d-8261-0c71eafbaa77",
            "compositeImage": {
                "id": "65739d33-7f74-42e2-ad9e-e4b9780c5cb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0acfc0b8-f677-4bda-8a90-243fb0b36cc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "467a0c11-6b2f-49ae-94eb-32cc76fc2d49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0acfc0b8-f677-4bda-8a90-243fb0b36cc2",
                    "LayerId": "04517902-6a85-443c-bd49-844eb01c02ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "04517902-6a85-443c-bd49-844eb01c02ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd1c1d09-8de8-494d-8261-0c71eafbaa77",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}