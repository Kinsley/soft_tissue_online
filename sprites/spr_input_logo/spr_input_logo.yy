{
    "id": "25129f65-ca45-4481-a298-a698cbf10ec5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_input_logo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "691673c7-ba77-4651-88e6-880789e0aff3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25129f65-ca45-4481-a298-a698cbf10ec5",
            "compositeImage": {
                "id": "8f731f24-4805-46a3-9675-e3464e0f0cf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "691673c7-ba77-4651-88e6-880789e0aff3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b80b220-f521-43bd-bdfe-68d5ad004e09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "691673c7-ba77-4651-88e6-880789e0aff3",
                    "LayerId": "182edfe9-4528-41ff-9528-5e115f96df03"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "182edfe9-4528-41ff-9528-5e115f96df03",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25129f65-ca45-4481-a298-a698cbf10ec5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}