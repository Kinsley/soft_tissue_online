{
    "id": "3694c749-3cb2-42db-b877-b607a0c5b7a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ami_start_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 8,
    "bbox_right": 24,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0c69738c-9d97-4cce-be62-0c919821536c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3694c749-3cb2-42db-b877-b607a0c5b7a1",
            "compositeImage": {
                "id": "018df0c6-2068-4110-be0e-09dd7bacbf06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c69738c-9d97-4cce-be62-0c919821536c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a505ab78-48a9-44c5-a702-843e43a70553",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c69738c-9d97-4cce-be62-0c919821536c",
                    "LayerId": "60892e08-daa7-45e7-9cc0-d10bc414ca54"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "60892e08-daa7-45e7-9cc0-d10bc414ca54",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3694c749-3cb2-42db-b877-b607a0c5b7a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}