{
    "id": "635e975e-ceb6-4691-88e0-54dc08e591cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ami_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 27,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "20654d5a-7fef-40ad-8e27-0f8778709699",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "635e975e-ceb6-4691-88e0-54dc08e591cf",
            "compositeImage": {
                "id": "92b91139-4e2e-43f7-a1e7-620df437dca5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20654d5a-7fef-40ad-8e27-0f8778709699",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0d6402c-2cb4-45cb-bf4b-b8633687eb31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20654d5a-7fef-40ad-8e27-0f8778709699",
                    "LayerId": "1c22e475-0dd4-4b3f-a4b6-77c9ea9d1139"
                }
            ]
        },
        {
            "id": "115a5b9e-6cca-4288-a9da-465c4f09c1fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "635e975e-ceb6-4691-88e0-54dc08e591cf",
            "compositeImage": {
                "id": "ada674a2-4c23-491a-a393-6595f1173856",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "115a5b9e-6cca-4288-a9da-465c4f09c1fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78db63ae-ce61-4638-9177-93e0e29efed0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "115a5b9e-6cca-4288-a9da-465c4f09c1fb",
                    "LayerId": "1c22e475-0dd4-4b3f-a4b6-77c9ea9d1139"
                }
            ]
        },
        {
            "id": "65142194-636d-4520-85b0-bb4df1977a01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "635e975e-ceb6-4691-88e0-54dc08e591cf",
            "compositeImage": {
                "id": "52d3742a-7294-4d26-94cb-a6994559ec48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65142194-636d-4520-85b0-bb4df1977a01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2913087-9d78-433e-9093-57d3da81b094",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65142194-636d-4520-85b0-bb4df1977a01",
                    "LayerId": "1c22e475-0dd4-4b3f-a4b6-77c9ea9d1139"
                }
            ]
        },
        {
            "id": "23b7d320-af8b-426f-8d12-df00c99d148f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "635e975e-ceb6-4691-88e0-54dc08e591cf",
            "compositeImage": {
                "id": "9a9e9897-30b1-4dad-bd84-c8939bd5df68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23b7d320-af8b-426f-8d12-df00c99d148f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3da282d3-7a9e-4a8e-b859-534546ba6cef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23b7d320-af8b-426f-8d12-df00c99d148f",
                    "LayerId": "1c22e475-0dd4-4b3f-a4b6-77c9ea9d1139"
                }
            ]
        },
        {
            "id": "e9ea3735-94fa-4ca5-93af-0ac81bb89595",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "635e975e-ceb6-4691-88e0-54dc08e591cf",
            "compositeImage": {
                "id": "9e5a9eab-8035-4e6f-8f85-814112f6ca20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9ea3735-94fa-4ca5-93af-0ac81bb89595",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59f10b0d-a062-479a-b36a-681a58e512a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9ea3735-94fa-4ca5-93af-0ac81bb89595",
                    "LayerId": "1c22e475-0dd4-4b3f-a4b6-77c9ea9d1139"
                }
            ]
        },
        {
            "id": "ef60ea29-4eba-4c62-b75d-805766057e27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "635e975e-ceb6-4691-88e0-54dc08e591cf",
            "compositeImage": {
                "id": "5259d73b-9003-4af0-b92b-123c858bec57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef60ea29-4eba-4c62-b75d-805766057e27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f02c9b6-e6d4-4723-9f82-e2830a0eb464",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef60ea29-4eba-4c62-b75d-805766057e27",
                    "LayerId": "1c22e475-0dd4-4b3f-a4b6-77c9ea9d1139"
                }
            ]
        },
        {
            "id": "32bb85f9-f943-4232-88e3-0354f872f0d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "635e975e-ceb6-4691-88e0-54dc08e591cf",
            "compositeImage": {
                "id": "0ddfafd3-143c-4be0-8657-b5f814e03bb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32bb85f9-f943-4232-88e3-0354f872f0d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f56067f9-b9a3-4f04-ab9e-60615a77e48f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32bb85f9-f943-4232-88e3-0354f872f0d8",
                    "LayerId": "1c22e475-0dd4-4b3f-a4b6-77c9ea9d1139"
                }
            ]
        },
        {
            "id": "bb0a049c-8e32-4994-b24c-4fc18875cf43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "635e975e-ceb6-4691-88e0-54dc08e591cf",
            "compositeImage": {
                "id": "3319730a-c68a-4ec3-ba6e-ee265bb06f5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb0a049c-8e32-4994-b24c-4fc18875cf43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "683f692a-dbf0-4686-addc-eb66060dd4c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb0a049c-8e32-4994-b24c-4fc18875cf43",
                    "LayerId": "1c22e475-0dd4-4b3f-a4b6-77c9ea9d1139"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1c22e475-0dd4-4b3f-a4b6-77c9ea9d1139",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "635e975e-ceb6-4691-88e0-54dc08e591cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}