{
    "id": "b2567b9d-a645-4d20-8cc3-eeb9954c6a39",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_music_player_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9554c686-1ca8-45d7-9789-27e77930b827",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2567b9d-a645-4d20-8cc3-eeb9954c6a39",
            "compositeImage": {
                "id": "4edd1781-6731-40df-b50b-494edea0de75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9554c686-1ca8-45d7-9789-27e77930b827",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "407eda35-d42d-47b7-b963-8b7ef30b201b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9554c686-1ca8-45d7-9789-27e77930b827",
                    "LayerId": "0344c0ba-d881-4c65-9021-8ed2836a9395"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0344c0ba-d881-4c65-9021-8ed2836a9395",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b2567b9d-a645-4d20-8cc3-eeb9954c6a39",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}