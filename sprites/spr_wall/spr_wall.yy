{
    "id": "27b2cc87-a268-4d6f-8604-d946301839c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ed6dbc4-38a5-4e53-8cd4-712cff068036",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27b2cc87-a268-4d6f-8604-d946301839c2",
            "compositeImage": {
                "id": "ea6b1f93-295f-43c5-b4d1-07c0dcf32782",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ed6dbc4-38a5-4e53-8cd4-712cff068036",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0b444f9-6131-47f0-a0c1-e2e8540cdd30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ed6dbc4-38a5-4e53-8cd4-712cff068036",
                    "LayerId": "a9f14db6-02a0-494e-b7c6-7f4c253573ae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a9f14db6-02a0-494e-b7c6-7f4c253573ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "27b2cc87-a268-4d6f-8604-d946301839c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 45,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}