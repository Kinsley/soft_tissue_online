{
    "id": "06e06d8b-4f19-4e97-95d0-83c5783a8b52",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ami_idle_shoot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 11,
    "bbox_right": 20,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a06251bc-419a-4a18-8a6b-77270f4f90e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06e06d8b-4f19-4e97-95d0-83c5783a8b52",
            "compositeImage": {
                "id": "54eddb1d-4401-4164-b82d-dae46c9506b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a06251bc-419a-4a18-8a6b-77270f4f90e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16f1a5b0-4ac1-4ea5-ade9-deec83c3df1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a06251bc-419a-4a18-8a6b-77270f4f90e1",
                    "LayerId": "2664e937-6758-4d4a-ad80-12f61b4a3d3c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2664e937-6758-4d4a-ad80-12f61b4a3d3c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "06e06d8b-4f19-4e97-95d0-83c5783a8b52",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}