{
    "id": "e00c280d-50b1-4439-9c83-528a4f2750c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ami_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 26,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21ebe7ac-e3ec-4ddd-a0c9-05259c7af159",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e00c280d-50b1-4439-9c83-528a4f2750c6",
            "compositeImage": {
                "id": "a125446a-f51c-4ee5-8216-f25995cfc44b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21ebe7ac-e3ec-4ddd-a0c9-05259c7af159",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d989a53-0001-423b-a3f0-6cbd57bd04ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21ebe7ac-e3ec-4ddd-a0c9-05259c7af159",
                    "LayerId": "b5e873bb-7aad-4226-81e6-9c0248d53398"
                }
            ]
        },
        {
            "id": "0fbcf69e-55f0-452a-ae3f-d6ae0877b27c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e00c280d-50b1-4439-9c83-528a4f2750c6",
            "compositeImage": {
                "id": "33551e32-faaa-4d6a-a2c7-30f3d70c5cba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fbcf69e-55f0-452a-ae3f-d6ae0877b27c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5cd7a42-79dd-462c-a99a-77d215cf8b9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fbcf69e-55f0-452a-ae3f-d6ae0877b27c",
                    "LayerId": "b5e873bb-7aad-4226-81e6-9c0248d53398"
                }
            ]
        },
        {
            "id": "f4857daf-6717-463f-9791-557a04509a87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e00c280d-50b1-4439-9c83-528a4f2750c6",
            "compositeImage": {
                "id": "d4191979-119c-4e72-8b90-461751933ec5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4857daf-6717-463f-9791-557a04509a87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fface4a-c75d-4c71-bf2f-3dd0302defb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4857daf-6717-463f-9791-557a04509a87",
                    "LayerId": "b5e873bb-7aad-4226-81e6-9c0248d53398"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b5e873bb-7aad-4226-81e6-9c0248d53398",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e00c280d-50b1-4439-9c83-528a4f2750c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}