{
    "id": "d7016e0f-d59c-443d-b93a-e11f45c4730b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lemon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 5,
    "bbox_right": 10,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a5652b79-d432-43b8-bf2c-ef7cd08620d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7016e0f-d59c-443d-b93a-e11f45c4730b",
            "compositeImage": {
                "id": "f6dd8a86-c94d-437a-96e8-b99324a5915c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5652b79-d432-43b8-bf2c-ef7cd08620d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73936491-7074-4b6d-b8e8-97c8c905eec8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5652b79-d432-43b8-bf2c-ef7cd08620d5",
                    "LayerId": "e7e22410-4242-4acb-a84d-7cf143fe05cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e7e22410-4242-4acb-a84d-7cf143fe05cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7016e0f-d59c-443d-b93a-e11f45c4730b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}