{
    "id": "f9ccb42b-74f3-492e-836a-e3c9c36d23f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_camera_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "53e8ac40-bacf-4bb2-a458-bd40b0e0a133",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9ccb42b-74f3-492e-836a-e3c9c36d23f8",
            "compositeImage": {
                "id": "ccfdf515-b22b-4c00-a2cc-d33902b30718",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53e8ac40-bacf-4bb2-a458-bd40b0e0a133",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d95a19c-bc38-4e80-a199-c46543898d8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53e8ac40-bacf-4bb2-a458-bd40b0e0a133",
                    "LayerId": "892fe7ab-5ac3-43cd-a386-248c1bafcf5f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "892fe7ab-5ac3-43cd-a386-248c1bafcf5f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f9ccb42b-74f3-492e-836a-e3c9c36d23f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}