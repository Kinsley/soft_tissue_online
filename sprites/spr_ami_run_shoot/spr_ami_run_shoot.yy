{
    "id": "2d9b4070-fd47-40c1-99a5-bf7e584d56f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ami_run_shoot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 27,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f886db4-7d3b-4fd4-b957-671471727fe8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d9b4070-fd47-40c1-99a5-bf7e584d56f7",
            "compositeImage": {
                "id": "76dfc870-b694-4811-8edc-7ed0d054ed1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f886db4-7d3b-4fd4-b957-671471727fe8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47001e02-4e4f-452b-8d7b-38bca71e97bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f886db4-7d3b-4fd4-b957-671471727fe8",
                    "LayerId": "815c994e-db4c-42c8-b7b2-b6336a794f87"
                }
            ]
        },
        {
            "id": "9aa9abbd-92f1-488b-a199-ddc0970a6eaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d9b4070-fd47-40c1-99a5-bf7e584d56f7",
            "compositeImage": {
                "id": "7dfa23ab-3310-4fca-a985-bd22876ed47a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9aa9abbd-92f1-488b-a199-ddc0970a6eaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f44c036-5c43-4da1-918d-c5670f646d67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9aa9abbd-92f1-488b-a199-ddc0970a6eaa",
                    "LayerId": "815c994e-db4c-42c8-b7b2-b6336a794f87"
                }
            ]
        },
        {
            "id": "76eb2b33-13cb-4540-ab96-480adb8428c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d9b4070-fd47-40c1-99a5-bf7e584d56f7",
            "compositeImage": {
                "id": "9ab7d04e-fccd-4f32-81d5-3f5b38d9ff6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76eb2b33-13cb-4540-ab96-480adb8428c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e56e27fb-7382-4c6c-bbfe-d034f0ebadec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76eb2b33-13cb-4540-ab96-480adb8428c8",
                    "LayerId": "815c994e-db4c-42c8-b7b2-b6336a794f87"
                }
            ]
        },
        {
            "id": "25d3f4c6-28de-49b8-b3cf-d484220d83d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d9b4070-fd47-40c1-99a5-bf7e584d56f7",
            "compositeImage": {
                "id": "aa26ddfd-d619-47d1-ad46-d19bc96c77f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25d3f4c6-28de-49b8-b3cf-d484220d83d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d254a25-617f-4afb-987f-7500dbf9b50f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25d3f4c6-28de-49b8-b3cf-d484220d83d1",
                    "LayerId": "815c994e-db4c-42c8-b7b2-b6336a794f87"
                }
            ]
        },
        {
            "id": "35dde24c-50dd-4b2e-85f5-8b8b4e00f09f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d9b4070-fd47-40c1-99a5-bf7e584d56f7",
            "compositeImage": {
                "id": "2cb1d0b1-a970-4d60-ba28-4d205ed97ff8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35dde24c-50dd-4b2e-85f5-8b8b4e00f09f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a34ad6a1-91d3-4d29-870d-32ccf53aebe8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35dde24c-50dd-4b2e-85f5-8b8b4e00f09f",
                    "LayerId": "815c994e-db4c-42c8-b7b2-b6336a794f87"
                }
            ]
        },
        {
            "id": "49fb9a9c-f123-4cdd-8e49-9640778b7869",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d9b4070-fd47-40c1-99a5-bf7e584d56f7",
            "compositeImage": {
                "id": "d1d225f9-0341-4f35-9864-2a5f308836a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49fb9a9c-f123-4cdd-8e49-9640778b7869",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d00022fd-ee5b-4183-a506-deff8a08693d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49fb9a9c-f123-4cdd-8e49-9640778b7869",
                    "LayerId": "815c994e-db4c-42c8-b7b2-b6336a794f87"
                }
            ]
        },
        {
            "id": "596ae321-6a59-49a0-ba96-6cb98e5612f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d9b4070-fd47-40c1-99a5-bf7e584d56f7",
            "compositeImage": {
                "id": "072e06e8-8f25-4550-a30c-ec2479f3b1a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "596ae321-6a59-49a0-ba96-6cb98e5612f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8300bd00-44ca-4a07-ba8c-e1029b94d79a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "596ae321-6a59-49a0-ba96-6cb98e5612f3",
                    "LayerId": "815c994e-db4c-42c8-b7b2-b6336a794f87"
                }
            ]
        },
        {
            "id": "ee251491-f1e4-4b96-aa5c-c0d7fcd3e45a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d9b4070-fd47-40c1-99a5-bf7e584d56f7",
            "compositeImage": {
                "id": "3057eeeb-8742-4485-9654-3a08fde92442",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee251491-f1e4-4b96-aa5c-c0d7fcd3e45a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4840f8a2-cfae-4ace-8402-538c7913a434",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee251491-f1e4-4b96-aa5c-c0d7fcd3e45a",
                    "LayerId": "815c994e-db4c-42c8-b7b2-b6336a794f87"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "815c994e-db4c-42c8-b7b2-b6336a794f87",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d9b4070-fd47-40c1-99a5-bf7e584d56f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}