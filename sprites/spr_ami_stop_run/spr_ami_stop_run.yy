{
    "id": "d4217d35-65c2-4a06-b913-28e7a11f4544",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ami_stop_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 8,
    "bbox_right": 25,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e379e9dd-5ea8-4211-bffa-35c1e062cf30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4217d35-65c2-4a06-b913-28e7a11f4544",
            "compositeImage": {
                "id": "185d873f-355f-4880-a7b1-518e20d96c93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e379e9dd-5ea8-4211-bffa-35c1e062cf30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaeedcfc-fa86-44fa-8267-168baf4a3838",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e379e9dd-5ea8-4211-bffa-35c1e062cf30",
                    "LayerId": "9b0f9007-ce48-4cb4-b98e-f29ef3da80b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9b0f9007-ce48-4cb4-b98e-f29ef3da80b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d4217d35-65c2-4a06-b913-28e7a11f4544",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}