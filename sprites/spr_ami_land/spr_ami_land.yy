{
    "id": "b1691ec9-9846-42ff-9b95-3c873ffdbc8a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ami_land",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 6,
    "bbox_right": 27,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "984b6f1a-145e-4655-96c5-340d3a594424",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1691ec9-9846-42ff-9b95-3c873ffdbc8a",
            "compositeImage": {
                "id": "61dbb5c2-1ae8-4b61-9404-4e429f706b91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "984b6f1a-145e-4655-96c5-340d3a594424",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0c0a355-653f-449a-aac9-f6ef56c92693",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "984b6f1a-145e-4655-96c5-340d3a594424",
                    "LayerId": "cf7aa336-8cbc-4fac-8cdc-cb39e4e2f563"
                }
            ]
        },
        {
            "id": "85da1208-0e55-4147-b3b1-fcc343181b39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1691ec9-9846-42ff-9b95-3c873ffdbc8a",
            "compositeImage": {
                "id": "89adaf35-2a19-4c58-bc9d-f36f7730adfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85da1208-0e55-4147-b3b1-fcc343181b39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e70d7a74-e2cc-454f-b2d3-2efae7381f48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85da1208-0e55-4147-b3b1-fcc343181b39",
                    "LayerId": "cf7aa336-8cbc-4fac-8cdc-cb39e4e2f563"
                }
            ]
        },
        {
            "id": "b65e7f57-9fd3-41d3-a723-c1e8d374288e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1691ec9-9846-42ff-9b95-3c873ffdbc8a",
            "compositeImage": {
                "id": "d0d57432-df80-4689-80f8-264ce8b104ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b65e7f57-9fd3-41d3-a723-c1e8d374288e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "110e8910-784e-4906-bc84-e5f928dbacbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b65e7f57-9fd3-41d3-a723-c1e8d374288e",
                    "LayerId": "cf7aa336-8cbc-4fac-8cdc-cb39e4e2f563"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cf7aa336-8cbc-4fac-8cdc-cb39e4e2f563",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1691ec9-9846-42ff-9b95-3c873ffdbc8a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}