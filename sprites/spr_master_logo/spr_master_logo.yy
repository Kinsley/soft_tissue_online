{
    "id": "197918fa-afa6-4ae9-a82a-0d54f8170c40",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_master_logo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "00bb6651-589e-4017-95ec-061d656c46ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "197918fa-afa6-4ae9-a82a-0d54f8170c40",
            "compositeImage": {
                "id": "8bca8a81-d81c-44b1-9f43-6db155091475",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00bb6651-589e-4017-95ec-061d656c46ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47dce125-84f3-4e4c-a8d1-c0e75201b309",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00bb6651-589e-4017-95ec-061d656c46ff",
                    "LayerId": "8a225a9d-642b-4200-9616-4c3c1e28156c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8a225a9d-642b-4200-9616-4c3c1e28156c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "197918fa-afa6-4ae9-a82a-0d54f8170c40",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}