{
    "id": "0b828d4e-05b9-409b-a041-1cb5c2b9b9c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ami_fall_shoot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 28,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "569e290e-5640-41ae-8dcf-a5613f3d9f90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b828d4e-05b9-409b-a041-1cb5c2b9b9c6",
            "compositeImage": {
                "id": "bc384c03-bcfb-42ec-996f-4d17e521d6af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "569e290e-5640-41ae-8dcf-a5613f3d9f90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce46a20e-45cf-423c-a507-f9ddd97b51a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "569e290e-5640-41ae-8dcf-a5613f3d9f90",
                    "LayerId": "7d5cf2a4-231f-4ba2-bedc-0d31cb3e07fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7d5cf2a4-231f-4ba2-bedc-0d31cb3e07fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b828d4e-05b9-409b-a041-1cb5c2b9b9c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}