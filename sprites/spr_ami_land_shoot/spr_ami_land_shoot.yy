{
    "id": "803c5684-d1e4-44d9-873d-6a200dab45b2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ami_land_shoot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 6,
    "bbox_right": 28,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d0ba1f2-e075-442b-aab3-1fdd7daac547",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "803c5684-d1e4-44d9-873d-6a200dab45b2",
            "compositeImage": {
                "id": "c9e37f7b-a042-4b25-a58e-5c07649eb164",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d0ba1f2-e075-442b-aab3-1fdd7daac547",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97c7a9e5-a470-4de1-a3cf-be5e9e529dc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d0ba1f2-e075-442b-aab3-1fdd7daac547",
                    "LayerId": "4e193032-c7bb-4c54-8287-fa81d878a465"
                }
            ]
        },
        {
            "id": "228de766-66e6-4ead-a0ac-fe9f76139344",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "803c5684-d1e4-44d9-873d-6a200dab45b2",
            "compositeImage": {
                "id": "db56e522-0f53-4cd1-9a71-cbdbb8677a03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "228de766-66e6-4ead-a0ac-fe9f76139344",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7250772b-5872-4aae-8d7d-6dd9178cfa1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "228de766-66e6-4ead-a0ac-fe9f76139344",
                    "LayerId": "4e193032-c7bb-4c54-8287-fa81d878a465"
                }
            ]
        },
        {
            "id": "326aba4c-6068-4052-b348-6abf28829929",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "803c5684-d1e4-44d9-873d-6a200dab45b2",
            "compositeImage": {
                "id": "735e0809-553b-41ad-855e-fdc5263cb421",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "326aba4c-6068-4052-b348-6abf28829929",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d507033d-2c2e-45b5-8313-b9a85bfdbf1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "326aba4c-6068-4052-b348-6abf28829929",
                    "LayerId": "4e193032-c7bb-4c54-8287-fa81d878a465"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4e193032-c7bb-4c54-8287-fa81d878a465",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "803c5684-d1e4-44d9-873d-6a200dab45b2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}