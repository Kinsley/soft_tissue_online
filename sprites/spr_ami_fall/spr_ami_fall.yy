{
    "id": "b655978b-204a-4c7e-9bbc-0a03a075c277",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ami_fall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 26,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff52fd99-96be-4754-a302-84858ed71c3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b655978b-204a-4c7e-9bbc-0a03a075c277",
            "compositeImage": {
                "id": "adf5db4c-8eaf-4502-8288-53bde915e7a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff52fd99-96be-4754-a302-84858ed71c3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4fd2ae3-8a6e-4e05-825f-171e8f591288",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff52fd99-96be-4754-a302-84858ed71c3b",
                    "LayerId": "7a2c7dd9-426c-4a54-8f8b-14c53f8a7d1f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7a2c7dd9-426c-4a54-8f8b-14c53f8a7d1f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b655978b-204a-4c7e-9bbc-0a03a075c277",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}