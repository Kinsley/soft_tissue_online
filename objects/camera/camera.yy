{
    "id": "174aadc8-8ade-473f-b9d5-85dba8522bb1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "camera",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "1a1e7c47-c71f-4648-8f91-4b4574cbed8b",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f9ccb42b-74f3-492e-836a-e3c9c36d23f8",
    "visible": false
}