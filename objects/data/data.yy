{
    "id": "044332bb-ba79-4e88-8f60-326d9c15db31",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "data",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "1a1e7c47-c71f-4648-8f91-4b4574cbed8b",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7a83ce88-ec2c-4e02-9507-265e2777a9c3",
    "visible": false
}