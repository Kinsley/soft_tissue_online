{
    "id": "0bff0e3d-d8b0-4cfd-9d4d-02a3a42a0b9e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "master",
    "eventList": [
        {
            "id": "e731e2aa-4f09-4238-a223-af62a40a847c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "0bff0e3d-d8b0-4cfd-9d4d-02a3a42a0b9e"
        },
        {
            "id": "1a2c06b5-a53c-436d-afaf-835ef15d0fb8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0bff0e3d-d8b0-4cfd-9d4d-02a3a42a0b9e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "197918fa-afa6-4ae9-a82a-0d54f8170c40",
    "visible": false
}