//Initialise Globals
init_globals();

//Load Audio Groups
audio_group_load(AG_BGM);
audio_group_load(AG_SFX);

var yy = 16;

//Instantiate Input
instance_create_layer(0,yy,layer,input);
yy+=16;

//Instantiate Data
instance_create_layer(0,yy,layer,data);
yy+=16;

//Instantiate Music Player
instance_create_layer(0,yy,layer,music_player);
yy+=16;

//Instantiate Sound Player
instance_create_layer(0,yy,layer,sound_player);
yy+=16;

//Instantiate Camera
instance_create_layer(0,yy,layer,camera);
yy+=16;



