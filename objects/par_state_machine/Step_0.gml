if(current_state != noone)
{
	if(event_based)
	{
		event_user(current_state);
	}
	else
	{
		if(script_exists(current_state))
		{
			script_execute(current_state);
		}
	}
}
