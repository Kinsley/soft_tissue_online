//This code means that ensures that state switches occur only once a frame 
//Provided that switch_state(state) is used correctly
//This also forces the flow of the state into the three positions: enter, run and exit
//And ensures that the next_state and target_state variables only store values in transition

if(next_state != noone)
{
	previous_state = current_state;
	current_state = next_state;
	next_state = noone;
	state_position = state_machine.enter_state;
}
else
{
	if(target_state != noone)
	{
		next_state = target_state;
		target_state = noone;
		state_position = state_machine.exit_state;
	}
	else
	{
		state_position = state_machine.run_state;
	}
}
