{
    "id": "7e61bec9-2cd8-4f09-8a39-4d3b75ad30b2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "par_state_machine",
    "eventList": [
        {
            "id": "1d13521e-2dcc-497a-9ad7-d8b5c1cb330b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7e61bec9-2cd8-4f09-8a39-4d3b75ad30b2"
        },
        {
            "id": "76677c43-4941-4d4b-9985-321a7fa7a4bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "7e61bec9-2cd8-4f09-8a39-4d3b75ad30b2"
        },
        {
            "id": "e8c46f0e-13c3-42b1-a44b-3c71766d6c03",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "7e61bec9-2cd8-4f09-8a39-4d3b75ad30b2"
        },
        {
            "id": "a20e7314-be64-44dd-a8e4-cc4b2219f467",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "7e61bec9-2cd8-4f09-8a39-4d3b75ad30b2"
        },
        {
            "id": "bddcb617-f63d-4807-b2d2-8627475532fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7e61bec9-2cd8-4f09-8a39-4d3b75ad30b2"
        },
        {
            "id": "e14924d4-32ac-4826-86d8-8bd8b07c983e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "7e61bec9-2cd8-4f09-8a39-4d3b75ad30b2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2abe0172-834f-421f-8f71-1277591ac447",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "ba39d384-8c7a-4a4f-a846-e9eb4e0bccef",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "event_based",
            "varType": 3
        },
        {
            "id": "31aa0e4c-6893-4e9d-814e-382e526cb63f",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "animation_lock",
            "varType": 3
        }
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}