{
    "id": "d45b78c6-a637-478c-b044-e9104d4188ac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_platform",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e0917d8c-3c97-4469-a8fd-a2d4a9a879f5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2d3c62cf-1630-444d-a34b-4738254d6010",
    "visible": false
}