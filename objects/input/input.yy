{
    "id": "3329a197-f025-41a6-99b9-b2f6bb7f0567",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "input",
    "eventList": [
        {
            "id": "e54cea0a-08bb-4da9-97f7-3a2ce8fe9919",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3329a197-f025-41a6-99b9-b2f6bb7f0567"
        },
        {
            "id": "d6bcacc5-fd54-4b7a-94de-6d9c685ee7b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "3329a197-f025-41a6-99b9-b2f6bb7f0567"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "1a1e7c47-c71f-4648-8f91-4b4574cbed8b",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "25129f65-ca45-4481-a298-a698cbf10ec5",
    "visible": false
}