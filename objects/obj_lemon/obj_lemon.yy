{
    "id": "260c384c-c310-49b4-87a4-21704aaaf4f2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lemon",
    "eventList": [
        {
            "id": "df19d264-0c3d-46f7-9d78-eab4c11c4959",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "260c384c-c310-49b4-87a4-21704aaaf4f2"
        },
        {
            "id": "d48f2319-7d91-4f63-8e72-3e10209cc0a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "260c384c-c310-49b4-87a4-21704aaaf4f2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d7016e0f-d59c-443d-b93a-e11f45c4730b",
    "visible": true
}