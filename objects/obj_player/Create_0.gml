/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

current_state = player_state_normal;
init_movable();

ghost_timer = 0;
ghost_time = 10;
jump_timer = 0;
jump_time = 5;
can_jump = false;
jump_counter = 0;

jspd = BASE_JUMP;

grounded = false;

has_shot = 0;
shot_time = 15;
shot_timer = 0;
shot_ox = 0;
shot_oy = 0;