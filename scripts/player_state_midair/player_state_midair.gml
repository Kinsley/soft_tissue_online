var temp_grav = grav;
var grounded = is_grounded();

if(grounded)
{
	accel = GROUND_FRICTION;
}
else
{
	accel = AIR_FRICTION;
}


player_movement();


/*
if(key_up_pressed)
{
	var ladder = instance_place(x,y,obj_ladder);
	if(ladder != noone)
	{
		get_on_ladder(ladder);
		exit;
	}
}*/


if(input.key_jump_released)
{
	can_jump = false;
	temp_grav = grav;
}

if(input.key_jump_held) && (can_jump)
{
	temp_grav = 0;
	if(jump_timer < 1)
	{
		jump_timer++;
	}
	else
	{
		jump_timer = 0;
		if(jump_counter < jump_time)
		{
			jump_counter++;
			vspd -= jspd;
		}
		else
		{
			can_jump = false;
		}
	}
}

apply_gravity(temp_grav);


player_shoot();

if(grounded)
{
	switch_state(player_state_normal);
	play_sound_interrupt(snd_land,200);
	ghost_timer = 0;
	can_jump = true;
	jump_timer = 0;
	jump_counter = 0;
}



collide_and_move();

