if(global.jump_buffer < JUMP_BUFFER_MAX)
{
	global.jump_buffer++;
}

if(keyboard_check_pressed(ord("Z")))
{
	global.jump_buffer = 0;
}


key_left = keyboard_check(vk_left);
key_right = keyboard_check(vk_right);
key_up = keyboard_check(vk_up);
key_down = keyboard_check(vk_down);

key_left_pressed = keyboard_check_pressed(vk_left);
key_right_pressed = keyboard_check_pressed(vk_right);
key_up_pressed = keyboard_check_pressed(vk_up);
key_down_pressed = keyboard_check_pressed(vk_down);

key_shoot = keyboard_check_pressed(ord("X"));

key_jump_pressed = (global.jump_buffer < JUMP_BUFFER_THRESHOLD)
key_jump_held = keyboard_check(ord("Z"));
key_jump_released = keyboard_check_released(ord("Z"));