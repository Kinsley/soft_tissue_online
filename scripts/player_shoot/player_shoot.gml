if(shot_timer <= 0)
{
	if(input.key_shoot)
	{
		has_shot = shot_time*2;
		shot_timer = shot_time;
		set_shot_offset();
		with(instance_create_depth(x+(shot_ox*sign(image_xscale))+hspd,y+shot_oy+vspd,depth-1,obj_lemon))
		{
			dir = sign(other.image_xscale);
		}
		play_sound_interrupt(snd_lemon,200);
	}
}
else
{
	shot_timer--;
}