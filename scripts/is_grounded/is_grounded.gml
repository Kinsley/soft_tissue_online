//=====================
// IS GROUNDED
//=====================
// Simplifies checking if an entity is on a solid object

var on_floor = false;

if(place_meeting(x,y+1,par_collision) || 
((collision_point(bbox_left,bbox_bottom+1,par_platform,false,true) != noone || collision_point(bbox_right,bbox_bottom+1,par_platform,false,true) != noone) && vspd >= 0))
{
	on_floor = true;
}

return on_floor