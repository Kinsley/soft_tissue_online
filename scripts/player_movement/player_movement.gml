///@arg acceleration (optional)

var acc = accel;

if(argument_count > 0)
{
	acc = argument[0];
}


var h = input.key_right - input.key_left;

if(h != 0)
{
	image_xscale = h;
}

hspd = approach(hspd,max_hspd*h,acc);

