//=====================
// PHYSICS CONSTANTS
//=====================
#macro BASE_GRAVITY 0.4
#macro GROUND_FRICTION 0.5
#macro AIR_FRICTION 0.3
#macro MAX_HSPEED 2
#macro MAX_VSPEED 5


//=====================
// PLAYER CONSTANTS
//=====================
#macro JUMP_BUFFER_MAX 30
#macro JUMP_BUFFER_THRESHOLD 5
#macro BASE_JUMP 0.65