grounded = is_grounded();
if(grounded)
{
	accel = GROUND_FRICTION;
}
else
{
	accel = AIR_FRICTION;
}