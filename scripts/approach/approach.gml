//==========================//
// APPROACH      			//
//==========================//
//Input: current_value, target_value, amount
//Effect: current_value is modified by the amount and never goes past the target_value
//Returns: modified current_value
//==========================//

///@arg current_value
///@arg target_value
///@arg amount

var current_value = argument0;
var target_value = argument1;
var amount = abs(argument2);

var new_value;


if(current_value < target_value)
{
	new_value = min(current_value+amount,target_value);
}
else
{
	if(current_value > target_value)
	{
		new_value = max(current_value-amount,target_value);
	}
	else
	{
		new_value = current_value;
	}
}

return new_value;