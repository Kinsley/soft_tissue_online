///@arg sleep_time

if(!instance_exists(obj_sleep))
{
	with(instance_create_depth(0,0,depth,obj_sleep))
	{
		global.control = false;
		timer = argument0;
	}
}