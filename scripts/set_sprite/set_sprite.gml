//==========================//
// SET SPRITE				//
//==========================//
//Input: New Sprite Index
//Effect: If the sprite_index of the object is not equal to the sprite_index passed
//in the objects sprite_index is set to the new sprite and sets the image_index to 0
//Returns: None
//==========================//

///@arg sprite_index
///@arg animation_lock (optional)

var new_sprite = argument[0];

if(sprite_index != new_sprite)
{
	sprite_index = new_sprite;
	image_index = 0;
	
	if(argument_count > 1)
	{
		animation_lock = argument[1];
	}
}

return;