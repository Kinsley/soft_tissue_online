///@arg sprite

var centre_x = noone;

if(argument_count > 0)
{
	var box_sprite = argument[0];
	var half_width = floor((sprite_get_bbox_right(box_sprite) - sprite_get_bbox_left(box_sprite))/2);
	centre_x = bbox_left + half_width;
}
else
{
	var half_width = floor((bbox_right - bbox_left)/2);
	centre_x = bbox_left + half_width;
}