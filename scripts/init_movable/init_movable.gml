///@arg accel (optional)
///@arg gravity (optional)
///@arg max_hspd (optional)
///@arg max_vspd (optional)


//Initialises an object to use the motion system
//either uses default values or optional values


hspd = 0;
vspd = 0;
vspd_fraction = 0;
hspd_fraction = 0;

accel = GROUND_FRICTION;
grav = BASE_GRAVITY;
max_hspd = MAX_HSPEED;
max_vspd = MAX_VSPEED;

if(argument_count > 0)
{
	accel = argument[0];
	
	if(argument_count > 1)
	{
		grav = argument[1];
			
		if(argument_count > 2)
		{
			max_hspd = argument[2];
			
			if(argument_count > 3)
			{
				max_vspd = argument[3];
			}
		}
	}
}





