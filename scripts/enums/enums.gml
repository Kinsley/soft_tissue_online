enum state_machine
{
	enter_state,
	run_state,
	exit_state,
	inactive_state
}