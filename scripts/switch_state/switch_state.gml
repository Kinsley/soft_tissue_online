///@arg next_state

var new_state = argument0;


if(target_state == noone)
{
	target_state = new_state;
		
	//DEBUG
	show_debug_message("STATE MACHINE: " + string(instance_id) + " transitioning from " 
						+ script_get_name(current_state) + " to " + script_get_name(target_state));
		
}
