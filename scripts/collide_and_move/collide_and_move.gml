vspd += vspd_fraction;
hspd += hspd_fraction;

vspd_fraction = frac(vspd);
hspd_fraction = frac(hspd);

vspd -= vspd_fraction;
hspd -= hspd_fraction;



if(place_meeting(x+hspd,y,par_collision))
{
	var h_step = sign(hspd);
	hspd = 0;
	hspd_fraction = 0;
	
	while(!place_meeting(x+sign(h_step),y,par_collision))
	{
		x += sign(h_step);
	}
}

if(place_meeting(x,y+vspd,par_collision))
{
	var v_step = sign(vspd);
	vspd = 0;
	vspd_fraction = 0;
	while(!place_meeting(x,y+sign(v_step),par_collision))
	{
		y+= sign(v_step);
	}
}

if(vspd > 0)
{
	if(collision_point(bbox_left,bbox_bottom+vspd,par_platform,false,true) != noone ||
		collision_point(bbox_left,bbox_bottom+vspd,par_platform,false,true) != noone)
	{
		var v_step = sign(vspd);
		vspd = 0;
		vspd_fraction = 0;
		while(collision_point(bbox_left,bbox_bottom+v_step,par_platform,false,true) == noone ||
		collision_point(bbox_left,bbox_bottom+v_step,par_platform,false,true) == noone)
		{
			y+= sign(v_step);
		}
	}
}



//Movement
x += hspd;
y += vspd;