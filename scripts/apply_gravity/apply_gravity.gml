///@arg gravity(optional)

//=================
// APPLY GRAVITY
//=================
// Input: gravity (optional)
// Effect: increases gravity if not on a collider or a platform


var g = grav;

if(argument_count > 0)
{
	g = argument[0];
}

if(!is_grounded())
{
	vspd = approach(vspd,max_vspd,g);
}

return;