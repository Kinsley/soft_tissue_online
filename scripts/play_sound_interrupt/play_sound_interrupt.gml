///@arg sound
///@arg priority

var snd = argument0;
var pri = argument1;

if(audio_is_playing(snd))
{
	audio_stop_sound(snd);
}


return audio_play_sound(snd,pri,false);