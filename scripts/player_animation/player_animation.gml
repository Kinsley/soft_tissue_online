if(!animation_lock)
{		
	var str = "";

	switch(current_state)
	{
		case player_state_normal:
			if(hspd == 0)
			{

				str = "spr_ami_idle";

			}
			else
			{
				str = "spr_ami_run";
			}
			break;		
			
		case player_state_midair:
			if(vspd > 0)
			{
				str = "spr_ami_fall";
			}
			else
			{
				str = "spr_ami_jump";
			}
	}
	
	
	if(has_shot > 0)
	{	
		str += "_shoot"
		has_shot--;
	}
	
	sprite_index = asset_get_index(str);

}