///@arg sprite

var centre_y = noone;
var half_height;


if(argument_count > 0)
{
	var box_sprite = argument[0];
	half_height = floor((sprite_get_bbox_bottom(box_sprite) - sprite_get_bbox_top(box_sprite))/2);
	centre_y = bbox_top + half_height;
}
else
{
	half_height = floor((bbox_bottom - bbox_right)/2);
	centre_y = bbox_top + half_height;
}